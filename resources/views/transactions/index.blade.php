@extends('layouts.app')

@if(Auth::user())
	@if(Auth::user()->role_id == 1)
		@section('content')
		<div class="ui container">
			<div class="ui doubling stackable grid container">
				<div class="four wide column">
					<div class="ui segment">
						<h1 class="ui header">Statuses</h1>
						<hr>
						<div class="ui middle aligned selection list">
							@foreach($statuses as $status)
							<a class="item" href="{{ route('transactions.index', ['status' => $status->id]) }}">
								<img class="ui avatar image" src="
								{{$status->id == 1 ? asset('images/pending.png') : '' }}
								{{$status->id == 2 ? asset('images/approved.png') : '' }}
								{{$status->id == 3 ? asset('images/returned.png') : '' }}
								{{$status->id == 6 ? asset('images/rejected1.png') : '' }}
								{{$status->id == 7 ? asset('images/canceled.png') : '' }}
								">
								<div class="content">
									<div class="header">{{ strtoupper($status->name) }}</div>
								</div>
							</a>
							@endforeach
						</div>
					</div>
				</div>
				<div class="twelve wide column">
					<div class="row">
						<div class="column">
							<h1>
							   {{!request()->status ? 'Pending' : ''}}
							   {{request()->status == 1 ? 'Pending' : ''}}
							   {{request()->status == 2 ? 'Approved' : ''}}
							   {{request()->status == 3 ? 'Returned' : ''}}
							   {{request()->status == 6 ? 'Rejected' : ''}}
							   {{request()->status == 7 ? 'Canceled' : ''}} Requests</h1>
						</div>
					</div>
					@section('scripts')
						@if(Session::has('request_approved'))
						<script>
							Swal.fire({
								position: 'top-end',
								icon: 'success',
								title: 'Request Approved!',
								showConfirmButton: false,
								timer: 1500
							})
						</script>
						@endif

						@if(Session::has('request_rejected'))
						<script>
							Swal.fire({
								position: 'top-end',
								icon: 'error',
								title: 'Request Rejected!',
								showConfirmButton: false,
								timer: 1500
							})
						</script>
						@endif

						@if(Session::has('unit_returned'))
						<script>
							Swal.fire({
								position: 'top-end',
								icon: 'success',
								title: 'Unit Returned!',
								showConfirmButton: false,
								timer: 1500
							})
						</script>
						@endif
					@endsection
					<div class="row">
						<table class="ui celled table text-center inverted 
							{{!request()->status ? 'teal' : ''}}
							{{request()->status == 1 ? 'teal' : ''}}
							{{request()->status == 2 ? 'green' : ''}}
							{{request()->status == 3 ? 'blue' : ''}}
							{{request()->status == 6 ? 'red' : ''}}
							{{request()->status == 7 ? 'purple' : ''}}
							table">
							<thead>
								<tr>
									<th>Reference_number</th>
									<th>Unit Name</th>
									<th>Request By</th>
									<th>Category</th>
									<th>Borrow Start</th>
									<th>Borrow End</th>
									@if(!request()->status ||request()->status == 1 || request()->status == 2)
									<th>Actions</th>
									@endif
									
								</tr>
							</thead>
							<tbody>
								@foreach($transactions as $transaction)
								<tr>
									<td>
										{{ $transaction->reference_number }}
									</td>
									<td>
										{{ $transaction->unit->name }}
									</td>
									<td>
										{{ $transaction->user->name }}
									</td>
									<td>
										{{ $transaction->unit->category->name }}
									</td>
									<td>
										{{ $transaction->borrowed_date->format('F d, Y') }}
									</td>
									<td>
										{{ $transaction->returned_date->format('F d, Y') }}
									</td>
									@if($transaction->status_id == 1)
									<td>
										<div class="ui small two buttons">
											<form action="{{ route('transactions.update', ['transaction' =>  $transaction->id ]) }}" method="POST">
												@csrf
												@method('PUT')
												<button class="green ui button"><i class="circle check outline icon"></i>Approve
												</button>
											</form>
											<form action="{{ route('transactions.destroy', ['transaction' => $transaction->id]) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="negative ui button"><i class="trash alternate outline icon"></i>Reject</button>
											</form>
										</div>
									</td>
									@elseif($transaction->status_id == 2)
									<td>
										<form action="{{ route('transactions.return', ['transaction' =>  $transaction->id ]) }}" method="POST">
											@csrf
											@method('PUT')
											<button class="yellow ui button"><i class="recycle icon"></i>Return
											</button>
										</form>
									</td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
						{{ $transactions->appends(request()->query())->links() }}
					</div>
				</div>
			</div>
		</div>
		@endsection

	@else
		@section('content')
			<div class="ui container">
				<div class="ui doubling stackable grid container">
					<div class="four wide column">
						<div class="ui segment">
							<h1 class="ui header">Statuses</h1>
							<hr>
							<div class="ui middle aligned selection list">
								@foreach($statuses as $status)
								<a class="item" href="{{ route('transactions.index', ['status' => $status->id]) }}">
									<img class="ui avatar image" src="
									{{$status->id == 1 ? asset('images/pending.png') : '' }}
									{{$status->id == 2 ? asset('images/approved.png') : '' }}
									{{$status->id == 3 ? asset('images/returned.png') : '' }}
									{{$status->id == 6 ? asset('images/rejected1.png') : '' }}
									{{$status->id == 7 ? asset('images/canceled.png') : '' }}
									">
									<div class="content">
										<div class="header">{{ strtoupper($status->name) }}</div>
									</div>
								</a>
								@endforeach
							</div>
						</div>
					</div>
					<div class="twelve wide column">
						<div class="row">
							<div class="column">
								<h1>My 
									   {{!request()->status ? 'Pending' : ''}}
									   {{request()->status == 1 ? 'Pending' : ''}}
									   {{request()->status == 2 ? 'Approved' : ''}}
									   {{request()->status == 3 ? 'Returned' : ''}}
									   {{request()->status == 6 ? 'Rejected' : ''}}
									   {{request()->status == 7 ? 'Canceled' : ''}} Requests</h1>
							</div>
						</div>

						<div class="row">
							<table class="ui celled table text-center inverted 
							{{!request()->status ? 'teal' : ''}}
							{{request()->status == 1 ? 'teal' : ''}}
							{{request()->status == 2 ? 'green' : ''}}
							{{request()->status == 3 ? 'blue' : ''}}
							{{request()->status == 6 ? 'red' : ''}}
							{{request()->status == 7 ? 'purple' : ''}}
							table">
								<thead>
									<tr>
										<th>Reference_number</th>
										<th>Unit Name</th>
										<th>Request By</th>
										<th>Category</th>
										<th>Borrow Start</th>
										<th>Borrow End</th>
										@if(!request()->status || request()->status == 1)
											<th>Action</th>
										@endif	
									</tr>
								</thead>
								<tbody>
									@foreach($transactions as $transaction)
									<tr>
										<td>
											{{ $transaction->reference_number }}
										</td>
										<td>
											{{ $transaction->unit->name }}
										</td>
										<td>
											{{ $transaction->user->name }}
										</td>
										<td>
											{{ $transaction->unit->category->name }}
										</td>
										<td>
											{{ $transaction->borrowed_date->format('F d, Y') }}
										</td>
										<td>
											{{ $transaction->returned_date->format('F d, Y') }}
										</td>
										@if(!request()->status || request()->status == 1)
										<td>
											<form action="{{ route('transactions.cancel', ['transaction' =>  $transaction->id ]) }}" method="POST">
												@csrf
												@method('PUT')
												<button class="red ui button"><i class="times circle outline icon"></i>Cancel
												</button>
											</form>
										</td>
										@endif
									</tr>
									@endforeach
								</tbody>
							</table>
							{{ $transactions->appends(request()->query())->links() }}
						</div>
					</div>
				</div>
			</div>
		@endsection
		@section('scripts')
			@if(Session::has('request_canceled'))
			<script>
				Swal.fire({
					position: 'top-end',
					icon: 'success',
					title: 'Request Canceled. Thank you!',
					showConfirmButton: false,
					timer: 1500
				})
			</script>
			@endif
		@endsection
	@endif
@else

@endif