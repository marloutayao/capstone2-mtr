@extends('layouts.app')


@if(Auth::user() && Auth::user()->role_id == 1)
	@section('content')
		<div class="ui container">
			<div class="ui doubling stackable grid container">
				<div class="four wide column">
					<div class="ui segment">
						<h1 class="ui header">Categories</h1>
						<hr>
						<div class="ui middle aligned selection list">
							@foreach($categories as $category)
							<a class="item" href="{{ route('units.index', ['category' => $category->id]) }}">
								<img class="ui avatar image" src="{{ url('/public/'.$category->image) }}">
								<div class="content">
									<div class="header">{{ $category->name }}</div>
								</div>
							</a>
							@endforeach
						</div>
					</div>
				</div>
				<div class="twelve wide column">
					<div class="row">
						<div class="column">
							<h1>Items</h1>
						</div>
						<div class="column right floated">
							<a href="{{ route('units.create') }}" class="green ui button"><i class="plus circle icon"></i>Add Unit</a>
						</div>
					</div>
					<div class="row">
						<table class="ui celled table text-center">
							<thead>
								<tr>
									<th>Image</th>
									<th>Name</th>
									<th>Serial Number</th>
									<th>Category</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($units as $unit)
								<tr>
									<td>
										<img class="ui tiny image centered" src="{{ url('/public/'.$unit->image) }}">
									</td>
									<td>{{ $unit->name }}</td>
									<td>{{ $unit->serial_number }}</td>
									<td>{{ $unit->category->name }}</td>
									<td>
										<div class="ui three buttons">
											<a class="green ui button" href="{{ route('units.show', ['unit' => $unit->id]) }}"><i class="newspaper outline icon"></i>Details
											</a>

											<form action="{{ route('units.destroy', ['unit' => $unit->id]) }}" method="POST">
												@csrf
												@method('DELETE')
												<button class="negative ui button"><i class="trash alternate outline icon"></i>Delete &nbsp;</button>

											</form>
											<a href="{{ route('units.edit', ['unit' =>  $unit->id ]) }}" class="teal ui button"><i class="edit outline icon"></i>Update</a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	@endsection

@else
	@section('content')
		<div class="ui container">
			<div class="ui doubling stackable grid container">
				<div class="four wide column">
					<div class="ui segment">
						<h1 class="ui header">Categories</h1>
						<hr>
						<div class="ui middle aligned selection list">
							@foreach($categories as $category)
							<a class="item" href="{{ route('units.index', ['category' => $category->id]) }}">
								<img class="ui avatar image" src="{{ url('/public/'.$category->image) }}">
								<div class="content">
									<div class="header">{{ $category->name }}</div>
								</div>
							</a>
							@endforeach
						</div>
					</div>
				</div>
				
				<div class="twelve wide column">
					<div class="row">
						<h1>Items</h1>
					</div>
					<hr>
					<div class="ui grid stackable">
						<div class="four column row doubling stackable">
							@foreach($units as $unit)
							<div class="column">
								<div class="ui fluid special cards centered">
									<div class="card mb-4">
										<div class="blurring dimmable image">
											<div class="ui dimmer">
												<div class="content">
													<div class="center">
														@if($unit->status->id != 5)
														<button type="button" class="ui teal button" data-toggle="modal" data-target="#show_request_modal{{$unit->id}}">
															<i class="add icon"></i>
															Request Unit
														</button>
														@else
														<label class="ui red label">
															<i class="times icon"></i>
															Not Available
														</label>
														@endif
													</div>
												</div>
											</div>
											<div class="ui fluid big image">
												<img style="height:200px;" src="{{ url('/public/'.$unit->image) }}">
											</div>
										</div>
										<div class="content">
											<div class="header">{{ $unit->name }}</div>
											<div class="description center aligned">
												<label class="ui label {{$unit->status->id == 5 ? 'yellow' : 'green'}} ">
													<i class="{{$unit->status->id == 5 ? 'times' : 'check'}} circle outline icon"></i>
													{{ strtoupper($unit->status->name) }}
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- The Modal -->
							<div class="modal fade" id="show_request_modal{{$unit->id}}">
								<div class="modal-dialog modal-dialog-centered show">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<h2 class="modal-title text-center">{{ $unit->name }}</h2>
											<hr>
											<div class="ui fluid cards centered">
												<div class="card mb-4">
													<div class=image">
														<div class="content">
															<div class="center">

															</div>
														</div>
														<div class="ui fluid big image">
															<img style="height:200px;" src="{{ url('/public/'.$unit->image) }}">
														</div>
													</div>
													<div class="content">
														<div class="header"></div>
														<div class="description center aligned">
															<label class="ui label green">
																<i class="check circle outline icon"></i>Available
															</label>
														</div>
													</div>


												</div>
											</div>
											<hr>
											<form action="{{ route('transactions.store') }}" method="POST">
												@csrf
												<input type="hidden" name="unit_id" value="{{ $unit->id }}">
												<input type="hidden" name="category_id" value="{{ $unit->category->id }}">
												<div class="ui form">
													<div class="two fields">
														<div class="field">
															<label>Borrowed Date Start</label>
															<input type="date" name="borrow_start" value="{{ old('borrow_start', date('m/d/Y')) }}" placeholder="Borrowed Date Start">
														</div>
														<div class="field">
															<label>Return Date</label>
															<input type="date" name="borrow_end" value="{{ old('borrow_end', date('m/d/Y')) }}" placeholder="Return Date">
														</div>
													</div>
												</div>
												<button class="btn btn-success btn-block">Request</button>
											</form>
										</div>
										<!-- Modal footer -->
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>        
										</div>
									</div>
								</div>
							</div>
							{{-- END OF MODAL --}}
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

@endif

@section('scripts')
@if(Session::has('deleted_message'))
	<script>
		Swal.fire({
			position: 'top-end',
			icon: 'error',
			title: 'Unit Deleted!',
			showConfirmButton: false,
			timer: 1500
		})
	</script>
@endif
@if(Session::has('request_success'))
<script>
	Swal.fire({
		position: 'top-end',
		icon: 'success',
		title: 'Request Success. Please wait for admin aproval, Thank you!',
		showConfirmButton: false,
		timer: 1500
	})
</script>
@endif

@if($errors->any())
<script>
	Swal.fire({
		position: 'top-end',
		icon: 'warning',
		title: "Request Failed. {{ $errors->first() }} Thank you!",
		showConfirmButton: false,
		timer: 6000
	})
</script>
@endif

<script>
	$('.special.cards .image').dimmer({
		on: 'hover'
	});
</script>
@endsection

