@extends('layouts.app')

@section('content')

<div class="ui raised very padded text container segment my-5">
    <h1 class="text-center">
        Details
    </h1>
    <hr>
    <div class="ui raised container segment">
    	<img class="ui centered medium image" src="{{ url('/public/'.$unit->image) }}">
    	

    </div>
    	<h3 class="ui header">Name</h3>
    	<div class="ui raised container segment">
    		  <strong> {{ $unit->name }} </strong>    		
    	</div>

		<h2 class="ui header">Serial Number: 
            <div class="ui olive label">
                {{ $unit->serial_number }}
            </div>
        </h2>
        
        <h2 class="ui header">Category:
            <div class="ui teal label">
                {{ $unit->category->name }}
            </div>
        </h2>

        <h2>Status:
            <div class="ui {{$unit->status->id == 4 ? 'green' : 'red' }} label">
                {{ strtoupper($unit->status->name) }}
            </div>  
        </h2>             		

    	<hr>
		<div class="ui center aligned basic segment">
        	<div class="big ui buttons">
        		<form action="{{ route('units.destroy', ['unit' => $unit->id]) }}" method="POST">
        			@csrf
        			@method('DELETE')
        			<button class="negative ui button"><i class="trash alternate outline icon"></i>Delete</button>

        		</form>
        		<div class="or"></div>
        		<a href="{{ route('units.edit', ['unit' =>  $unit->id ]) }}" class="teal ui button"><i class="edit outline icon"></i>Update</a>
        	</div>
    	</div>
</div>

@endsection