@extends('layouts.app')

@section('content')

<div class="ui raised very padded text container segment my-5">
    <h1 class="text-center">
        {{ isset($unit) ? 'Edit Unit' : 'Add New Unit' }}
    </h1>
    <hr>
    
    @if ($errors->any())
        <div class="ui error message">
            <div class="header">
                There were error with your submission
            </div>
            <ul class="list">
                @foreach($errors->all() as $error)
                    {{-- <li>{{ $errors->all() }}</li> --}}
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" method="POST" action="{{ isset($unit) ? route('units.update', ['unit' => $unit->id   ]) : route('units.store') }}" enctype="multipart/form-data">
        @csrf
        @if(isset($unit))
            @method('PUT')
        @endif
        <div class="field">
            <label for="category">Unit Name</label>
            <input type="text" name="name" placeholder="Unit Name" 
            value="{{ isset($unit) ? $unit->name : old('name') }}"
            >
        </div>

        <div class="field">
            <label for="serial_number">Serial Number</label>
            <input name="serial_number" placeholder="Serial Number...." value="{{ isset($unit) ? $unit->serial_number : old('serial_number') }}">
        </div>

        <div class="field">
            <label for="category_id">Category</label>
            <select name="category" id="category_id">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" 
                        @if( isset($unit))
                        {{$unit->category_id == $category->id ? 'selected' : ''}}
                        @endif
                        >
                        {{ strtoupper($category->name) }}
                    </option>
                @endforeach

            </select>
        </div>

        @if(isset($unit))
            <img class="ui medium centered image fluid" src="{{ url('/public/'.$unit->image) }}">
        @endif

        <div class="ui float-left transparent field left icon input">
            <input type="file" class="form-control-file" name="image" placeholder="Search...">
            <i class="image icon"></i>
        </div>  
        <button class="ui teal large button right floated" type="submit">
            {{ isset($unit) ? 'Update' : 'Add Unit' }}
        </button>
    </form>
</div>

@endsection

@section('scripts')

    @if(Session::has('unit_uploaded'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Unit Added',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    @if(Session::has('edit_unit'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Unit Updated!',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    @if(Session::has('no_changes'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'info',
                title: 'There were no changes in the unit details!',
                showConfirmButton: false,
                timer: 3000
            })
        </script>
    @endif



@endsection