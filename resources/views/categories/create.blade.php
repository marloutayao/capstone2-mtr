@extends('layouts.app')

@section('content')

<div class="ui raised very padded text center aligned container segment my-5">
    <h1 class="center">
        {{ isset($category) ? 'Edit Category' : 'Add New Category' }}
    </h1>
    <hr>
    @if ($errors->any())
    <div class="ui error message">
        <div class="header">
            There were error with your submission
        </div>
        <ul class="list">
            @foreach($errors->all() as $error)
                {{-- <li>{{ $errors->all() }}</li> --}}
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form class="ui form" method="POST" action="{{ isset($category) ? route('categories.update', ['category' => $category->id   ]) : route('categories.store') }}" enctype="multipart/form-data">
        @csrf
        @if(isset($category))
            @method('PUT')
        @endif
        <div class="field">
            <label for="category">Category Name:</label>
            <input type="text" name="name" placeholder="Category Name" 
            value="{{ isset($category) ? $category->name : old('name') }}"
            >
        </div>

        <div class="field">
            <label for="description">Description</label>
            <textarea name="description" cols="30" rows="10" placeholder="Description....">{{ isset($category) ? $category->description : old('description') }}</textarea>
        </div>
        <div class="ui float-left transparent field left icon input">
            <input type="file" class="form-control-file" name="image" placeholder="Search...">
            <i class="image icon"></i>
        </div>  
        <button class="ui teal large button right floated" type="submit">
            {{ isset($category) ? 'Update' : 'Add Category' }}
        </button>
    </form>
</div>

@endsection

@section('scripts')

    @if (Session::has('category_uploaded'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Category Uploaded!',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

@endsection