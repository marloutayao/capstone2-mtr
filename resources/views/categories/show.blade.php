@extends('layouts.app')

@section('content')

<div class="ui raised very padded text container segment my-5">
    <h1 class="text-center">
        Details
    </h1>
    <hr>
    <div class="ui raised container segment">
    	<img class="ui centered medium image" src="{{ url('/public/'.$category->image) }}">
    	

    </div>
    	<h3 class="ui header">Name</h3>
    	<div class="ui raised container segment">
    		  <strong> {{ $category->name }} </strong>    		
    	</div>

		<h2 class="ui header">Description</h2>
    	<div class="ui raised container segment">
    		<p>{{ $category->description }}</p>    		
    	</div>
    	<hr>

		<div class="ui center aligned basic segment">
    	<div class="big ui buttons">
    		<form action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="POST">
    			@csrf
    			@method('DELETE')
    			<button class="negative ui button"><i class="trash alternate outline icon"></i>Delete</button>

    		</form>
    		<div class="or"></div>
    		<a href="{{ route('categories.edit', ['category' =>  $category->id ]) }}" class="teal ui button"><i class="edit outline icon"></i>Update</a>
    	</div>
    	</div>
</div>

@endsection