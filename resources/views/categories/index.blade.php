@extends('layouts.app')

@section('content')
    <div class="ui segment container">
        <h1 class="text-center">CATEGORIES</h1>
        <div class="ui grid">
            <div class="sixteen wide mobile eight wide computer column">
                <img class="ui fluid image centered" src="{{ asset('images/categories_img.png') }}">
            </div>
            <div class="sixteen wide mobile eight wide computer column mt-5">

                <a class="ui black link" id="add-category" name="add-category" href="{{ route('categories.create') }}">
                <div class="ui right floated small teal labeled icon button mb-2">
                    <i class="plus circle icon"></i>
                        Add Category
                </div>
                </a>
                <table class="ui table">
                    <thead>
                        <tr>
                            <th class="four wide">Name</th>
                            <th class="six wide text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                        <tr @if(Session::has('category_updated') && Session::get('category_updated') == $category->id)
                            class='ui inverted olive table'
                            @endif>
                            <th>{{ $category->name }}</th>
                            <td>
                                <div class="ui three buttons">
                                    <a class="green ui button" href="{{ route('categories.show', ['category' => $category->id]) }}"><i class="newspaper outline icon"></i>Details</a>
                                    <form action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="negative ui button"><i class="trash alternate outline icon"></i>Delete</button>
                                    
                                    </form>
                                    <a href="{{ route('categories.edit', ['category' =>  $category->id ]) }}" class="teal ui button"><i class="edit outline icon"></i>Update</a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    @if(Session::has('category_updated'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Category Updated!',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif

    @if(Session::has('deleted_message'))
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Category Deleted!',
                showConfirmButton: false,
                timer: 1500
            })
        </script>            
    @endif

@endsection