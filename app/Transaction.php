<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $fillable = ['reference_number', 'unit_id', 'user_id', 'category_id', 'staus_id', 'borrowed_date', 'returned_date'];
    
    protected $dates = ['borrowed_date', 'returned_date'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function status(){
    	return $this->belongsTo('App\Status');
    }

    public function unit(){
        return $this->belongsTo('App\Unit');
    }

    // many to many relation ship
    public function units(){
        return $this->belongsToMany('App\Unit', 'transaction_unit')
            ->withPivot('borrow_start', 'borrow_end', 'unit_id')
            ->withTimestamps();
    }
}
