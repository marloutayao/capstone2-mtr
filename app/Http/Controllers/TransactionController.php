<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Unit;
use Auth;
use Str;
use App\Status;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Unit $unit, Request $request, Transaction $transaction)
    {   
        $this->authorize('view', $transaction);

        $statuses = [];
        $showTransaction = [];
        
        if(Auth::user()){
            if(Auth::user()->role_id == 1){
                $statuses = Status::whereIn('id', [1,2,3,6,7])->get();
                $showTransaction = Transaction::where('status_id', 1)->paginate(7);
                if ($request->status) {
                    if ($request->status == 1){
                        $showTransaction = Transaction::where('status_id', 1)->paginate(7);
                    } else if ($request->status == 2) {
                        $showTransaction = Transaction::where('status_id', 2)->paginate(7);
                    } else if ($request->status == 3) {
                        $showTransaction = Transaction::where('status_id', 3)->paginate(7);
                    } else if ($request->status == 6) {
                        $showTransaction = Transaction::where('status_id', 6)->paginate(7);
                    } else if ($request->status == 7) {
                        $showTransaction = Transaction::where('status_id', 7)->paginate(7);
                    }
                }
            } else {
                $statuses = Status::whereIn('id', [1,2,3,6,7])->get();
                $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 1)->paginate(7);
                if ($request->status) {
                    if ($request->status == 1){
                        $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 1)->paginate(7);
                    } else if ($request->status == 2) {
                        $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 2)->paginate(7);
                    } else if ($request->status == 3) {
                        $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 3)->paginate(7);
                    } else if ($request->status == 6) {
                        $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 6)->paginate(7);
                    } else if ($request->status == 7) {
                        $showTransaction = Transaction::where('user_id', Auth::user()->id)->where('status_id', 7)->paginate(7);
                    }
                }
            }
        }

        return view('transactions.index')
        ->with('statuses', $statuses )
        ->with('transactions', $showTransaction);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('errors.404');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Unit $unit, Transaction $transaction)
    {
        $this->authorize('create', $transaction);
        $user_id = Auth::user()->id;
        // date('Y-m-d-H-i-s')
        $reference_number = date('Y-m-d-(H:i:s)'). "-MTR-".Str::random(13);
        $request->validate([
            'borrow_start' => 'required|date',
            'borrow_end' => 'required|date|after:borrow_start'
        ]);

        // Transaction::create([
        //     'reference_number' => $reference_number,
        //     'unit_id' => $request->input('unit_id'),
        //     'user_id' => $user_id,
        //     'category_id' => $request->input('category_id'),
        //     'status_id' => 1,
        //     'borrowed_date' => $request->input('borrow_start'),
        //     'returned_date' => $request->input('borrow_end')
        // ]);

        $transaction->reference_number = $reference_number;
        $transaction->unit_id = $request->input('unit_id');
        $transaction->user_id = $user_id;
        $transaction->category_id = $request->input('category_id');
        $transaction->status_id = 1;
        $transaction->borrowed_date = $request->input('borrow_start');
        $transaction->returned_date = $request->input('borrow_end');
        $transaction->save();        

        $transaction->units()->attach(
            $request->input('unit_id'),
            [
                'borrow_start' => $request->input('borrow_start'),
                'borrow_end' => $request->input('borrow_end'),
                'transaction_id' => $transaction->id

            ]
        );

        $transaction->save();

        // $unit->where('id', $request->input('unit_id'))
        //     ->update(['status_id' => 5]);


        return redirect(route('units.index'))->with('request_success', 'Please wait for admin approval, Thank you!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return view('errors.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        return view('errors.404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction, Unit $unit)
    {
        $this->authorize('update', $transaction);
        $transaction->status_id = 2;
        $transaction->save();

        $unit->where('id', $transaction->unit->id)
            ->update(['status_id' => 5]);

        return redirect(route('transactions.index'))->with('request_approved', 'APPROVED');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction, Unit $unit)
    {
        $this->authorize('delete', $transaction);
        $transaction->status_id = 6;
        $transaction->save();

        $unit->where('id', $transaction->unit->id)
            ->update(['status_id' => 4]);

        return redirect(route('transactions.index'))->with('request_rejected', 'REJECTED');
    }

    public function returnUnit(Transaction $transaction, Unit $unit) {
        $this->authorize('returnUnit', $transaction);
        $transaction->status_id = 3;
        $transaction->save();

        $unit->where('id', $transaction->unit->id)
            ->update(['status_id' => 4]);

        return redirect()->back()->with('unit_returned', 'RETURNED');
    }

    public function cancelRequest(Transaction $transaction, Unit $unit) {
        $this->authorize('cancelRequest', $transaction);
        $transaction->status_id = 7;
        $transaction->save();

        $unit->where('id', $transaction->unit->id)
            ->update(['status_id' => 4]);

        return redirect()->back()->with('request_canceled', 'CANCELED');
    }
}
