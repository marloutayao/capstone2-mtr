<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $this->authorize('view', $category);
        return view('categories.index', ['categories' => Category::all() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $this->authorize('view', $category);
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->authorize('view', $category);
        $request->validate([
            'name' => 'required|string|unique:categories,name|min:3',
            'description' => 'required|string',
            'image' => 'required|image|max:3000|mimes:jpeg,jpg,png,gif',
        ]);

        $file = $request->file('image');
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_extension = $file->extension();
        
        $random_name = Str::random(10);

        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images', $new_file_name, 'public');

        
        Category::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image' => $filepath
        ]);

        return redirect( route('categories.create') )->with('category_uploaded', 'Category Uploaded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $this->authorize('view', $category);
        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('view', $category);
        return view('categories.create')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->authorize('view', $category);
        $request->validate([
           // 'name' => 'required|string|unique:categories,name|min:3',
           'description' => 'required|string',
        ]);
        
        $file = $request->file('image');

        if($file){
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_extension = $file->extension();

            $random_name = Str::random(10);

            $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

            $filepath = $file->storeAs('images', $new_file_name, 'public');
            $category->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'image' => $filepath
            ]);    
        } else {
            $category->update([
                'name' => $request->input('name'),
                'description' => $request->input('description')
            ]);    
        }
        
        return redirect( route('categories.index'))
            ->with('category_updated', $category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->authorize('view', $category);
        $category->delete();
        return redirect( route('categories.index'))->with('deleted_message', 'Category Deleted');
    }
}
