<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Unit $unit)
    {
        // $this->authorize('view', $unit);
        
        if ($request->has('category')) {
            $showQuery = $unit->all()->where('category_id', '=', $request->category);
        } else {
            $showQuery = $unit::all();
        }

        return view('units.index')
            ->with('categories', Category::all())
            ->with('units', $showQuery);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Unit $unit)
    {
        $this->authorize('create', $unit);
        return view('units.create')->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Unit $unit)
    {
        $this->authorize('create', $unit);
        
        $request->validate([
            'name' => 'required|min:3|string',
            'serial_number' => 'required|string|min:6',
            'image' => 'required|image|max:3000|mimes:jpeg,jpg,png,gif',
            'category' => 'required'
        ]);

        $file = $request->file('image');
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_extension = $file->extension();
        
        $random_name = Str::random(10);

        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images', $new_file_name, 'public');

        Unit::create([
            'name' => $request->input('name'),
            'serial_number' => $request->input('serial_number'),
            'image' => $filepath,
            'category_id' => $request->input('category')
        ]);

        return redirect( route('units.create') )->with('unit_uploaded', 'UPLOADED');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Units  $units
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        $this->authorize('view', $unit);
        return view('units.show')->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Units  $units
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        $this->authorize('update', $unit);
        return view('units.create')
            ->with('unit', $unit)
            ->with('categories', Category::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Units  $units
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $this->authorize('update', $unit);
        $request->validate([
            'name' => 'required|min:3|string',
            'serial_number' => 'required|string|min:6',
        ]);

        if($unit->name == $request->input('name') &&
            $unit->serial_number == $request->input('serial_number') &&
            $unit->category_id == $request->input('category') &&
            !$request->hasFile('image')
        ) {
            $request->session()->flash('no_changes', 'No changes');
        }
        else {
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_extension = $file->extension();
                
                $random_name = Str::random(10);

                $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_name . "_" . $file_name . "." . $file_extension;

                $filepath = $file->storeAs('images', $new_file_name, 'public');

                $unit->update([
                    'image' => $filepath
                ]);
            }
            $unit->update([
                'name' => $request->input('name'),
                'serial_number' => $request->input('serial_number'),
                'category_id' => $request->input('category')
            ]);
            $request->session()->flash('edit_unit', 'edit_unit');
        }
            return redirect( route('units.edit', ['unit' => $unit->id]) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Units  $units
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {   
        $this->authorize('delete', $unit);
        //delete function
        $unit->delete();
        return redirect( route('units.index'))->with('deleted_message', 'Unit Deleted');
    }

}
