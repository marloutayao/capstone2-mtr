<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'serial_number', 'image', 'category_id'];

    public function category(){
    	return $this->belongsTo('App\Category');
    }
    
    public function status(){
    	return $this->belongsTo('App\Status');
    }

    public function transaction(){
    	return $this->belongsTo('App\Transaction');
    }
}
