<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::put('transactions/returnUnit/{transaction}', 'TransactionController@returnUnit')->name('transactions.return');
Route::put('transactions/cancelUnit/{transaction}', 'TransactionController@cancelRequest')->name('transactions.cancel');

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('categories', 'CategoryController');
Route::resource('units', 'UnitController');
Route::resource('transactions', 'TransactionController');
