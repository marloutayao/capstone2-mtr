<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'name' => 'Sport Court',
        	'description' => '',
        	'image' => 'images/sport.png'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Rest House',
        	'description' => '',
        	'image' => 'images/rest.png'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Event Place',
        	'description' => '',
        	'image' => 'images/event.png'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Meeting Place',
        	'description' => '',
        	'image' => 'images/meeting.png'
        ]);

    }
}
