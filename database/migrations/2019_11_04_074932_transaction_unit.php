<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TransactionUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_unit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('borrow_start');
            $table->dateTime('borrow_end');

            $table->unsignedBigInteger('unit_id');
            $table->foreign('unit_id')
                ->references('id')->on('units')
                ->onDelete('restrict')
                ->onDelete('cascade');

            $table->unsignedBigInteger('transaction_id');
            $table->foreign('transaction_id')
                ->references('id')->on('transactions')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
