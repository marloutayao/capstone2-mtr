<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number');

            $table->unsignedBigInteger('unit_id')->nullable(true);
                $table->foreign('unit_id')
                    ->references('id')->on('units')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->unsignedBigInteger('user_id')->nullable(true);
                $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->unsignedBigInteger('category_id')->nullable(true);
                $table->foreign('category_id')
                    ->references('id')->on('categories')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->unsignedBigInteger('status_id')->default(1);
                $table->foreign('status_id')
                    ->references('id')->on('statuses')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');

            $table->dateTime('borrowed_date');
            $table->dateTime('returned_date');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
